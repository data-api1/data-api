<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Repositories\Interfaces\ClientInterface;


class UserController extends Controller
{
    private $clientRepository;

    public function __construct(ClientInterface $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }
    public function insert_user(Request $request){ 
        
        $json_data = $request->json()->all();
        //validations
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'address1' => 'required',  
            'address2' => 'required',
            'city'  => 'required',
            'state' => 'required'        ,
            'country' => 'required',
            'zipcode' =>'required|numeric',
            'phoneNo1' => 'required|',
            'phoneNo2' => 'required|',
            'user.firstName' => 'required',
            'user.lastName' => 'required',
            'user.email' => 'required|unique:users,email',
            'user.password' => 'required|same:user.passwordConfirmation',
            'user.phone' => 'required',
        ]);
        if ($validator->fails()) {
            
            $responseArr['error'] = $validator->errors();;
            // $responseArr['token'] = '';
            return response()->json($responseArr);
        }

        $insert_data = $this->clientRepository->insert($json_data);                     
        //client data
        
        return $insert_data;
    }

    public function get_data(Request $request){   
        $clients = $this->clientRepository->all($request);                     
        return response()->json($clients);
    }
}
