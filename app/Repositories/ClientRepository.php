<?php

namespace App\Repositories;

use Carbon\Carbon;
use App\Models\Client;
use App\Models\User;
use App\Repositories\Interfaces\ClientInterface;
use Illuminate\Support\Facades\Redis;

class ClientRepository implements ClientInterface
{
    public function all($request)
    {   $clients = new Client;
        if($request->filter && $request->filter_data){
            $clients = $clients->where($request->filter,'Like','%'.$request->filter_data.'%');
        }
        if($request->sort && $request->direction){
            $clients = $clients->orderBy($request->sort,$request->direction); 
        }        
        return $clients->paginate(10);
    }

    public function insert($json_data)
    {
        $name = $json_data['name'];
        $address1 = $json_data['address1'];
        $address2 = $json_data['address2'];
        $city = $json_data['city'];
        $state = $json_data['state'];
        $country = $json_data['country'];
        $zipcode = $json_data['zipcode'];        
        $phoneno1 = $json_data['phoneNo1'];
        $phoneno2 = $json_data['phoneNo2'];
        $start_validity = Carbon::now();
        $end_validity = Carbon::now()->addDays(15);
        $status = "Active";

        //creating address
        $address = urlencode($address1." ".$address2." ".$city." ".$state." ".$country);        

        //Redis logic
        if( Redis::get($address)) {
            $final_latlng = Redis::get($address);                                    
            
        }else{            
            $getlatlong = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=".$address."&key=AIzaSyAk_Mx14xGkCp5I6FPpSUmaz6fnCGox7Mc");            
            $lat_long = json_decode($getlatlong);            
            $final_latlng = json_encode(array($lat_long->results[0]->geometry->location->lat,$lat_long->results[0]->geometry->location->lng));                            
            Redis::set($address,$final_latlng);                        
        }
        $final_latlng = json_decode($final_latlng);        
        $lat = $final_latlng[0];        
        $long = $final_latlng[1];
        
        
        // user data
        $fname = $json_data['user']['firstName'];
        $lname = $json_data['user']['lastName'];
        $email = $json_data['user']['email'];
        $password = $json_data['user']['password'];
        $password_c = $json_data['user']['passwordConfirmation'];
        $phone = $json_data['user']['phone'];
        //status???

        $client = new Client;
        $client->client_name = $name;
        $client->address1 = $address1;
        $client->address2 = $address2;
        $client->city = $city;
        $client->state = $state;
        $client->country = $country;
        $client->latitude = $lat;
        $client->longitude = $long;
        $client->zip = $zipcode;
        $client->phone_no1 = $phoneno1;
        $client->phone_no2 = $phoneno2;
        $client->start_validity = $start_validity;
        $client->end_validity = $end_validity;
        $client->status = $status;
        // dd($client);
        $client->save();

        $user_array = new User(
            [
                'first_name'=>$fname, 
                'last_name'=>$lname,
                'email'=>$email,
                'password'=>$password,
                'phone'=>$phone,
                'status'=>$status,
            ]
     );

     $client->user()->save($user_array);
     return json_encode(array("success"=>"User added successfully."));
    }
}