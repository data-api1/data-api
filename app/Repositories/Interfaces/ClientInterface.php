<?php

namespace App\Repositories\Interfaces;

use App\Models\User;
use App\Models\Client;

interface ClientInterface
{
    public function all($request);

    public function insert($data);
}