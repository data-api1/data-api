<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->string('client_name',100)->nullable();
            $table->text('address1')->nullable();
            $table->text('address2')->nullable();
            $table->string('city',100)->nullable();
            $table->string('state',100)->nullable();
            $table->string('country',100)->nullable();
            $table->double('latitude', 8, 3)->nullable();
            $table->double('longitude', 8, 3)->nullable();
            $table->string('phone_no1',20)->nullable();
            $table->string('phone_no2',20)->nullable();
            $table->string('zip',20)->nullable();
            $table->date('start_validity')->nullable();
            $table->date('end_validity')->nullable();
            $table->enum('status', ['Active', 'Inactive'])->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
