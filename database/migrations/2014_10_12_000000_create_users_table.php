<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->unsignedBigInteger('client_id');
            $table->string('first_name',50)->nullable();
            $table->string('last_name',50)->nullable();
            $table->string('email',150)->unique();
            // $table->timestamp('email_verified_at')->nullable();
            $table->string('password',256);
            $table->string('phone',20)->nullable();
            $table->string('profile_uri',255)->nullable();
            
            $table->timestamp('last_password_reset')->useCurrent();
            $table->enum('status', ['Active', 'Inactive'])->nullable();
            // $table->rememberToken();            
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
